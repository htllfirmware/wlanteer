BEGIN{
  FS=","
  OFS=""
  #getline #BEGIN runs before anything else, so grab the first line with the titles right away
  #getline
  #for(i=1;i<=NF;i++){
#	gsub("/[^a-z0-9]/i","",$i);
 #   names[i] = ($i)
 # }
#  names = array["bssid", "firstSeen", "lastSeen", "channel", "speed", "privacy", "cipher", "auth", "power", "beacons", "iv", "ip", "IsLength", "essid"]
  names1[1] = "bssid"
  names1[2] = "firstSeen"
  names1[3] = "lastSeen"
  names1[4] = "channel"
  names1[5] = "speed"
  names1[6] = "privacy"
  names1[7] = "cipher"
  names1[8] = "auth"
  names1[9] = "power"
  names1[10] = "beacons"
  names1[11] = "iv"
  names1[12] = "ip"
  names1[13] = "isLength"
  names1[14] = "essid"

  #Station MAC, First time seen, Last time seen, Power, # packets, BSSID, Probed ESSIDs
  names2[1] = "mac"
  names2[2] = "firstSeen"
  names2[3] = "lastSeen"
  names2[4] = "power"
  names2[5] = "packets"
  names2[6] = "bssid"
  names2[7] = "essid"

  printf "["
}
{
  if(($1) == "Station MAC") next
  if(($1) == "BSSID") next
  if(NF == 15){
    printf "{"
    for(i=1;i<=NF -1;i++)
    {
      gsub(/[ \t]+$/, "", $(i));
      gsub(/^[ \t]+/, "", $(i));
      printf "\"%s\": \"%s\"%s",names1[i],($i),(i == NF - 1 ? "" : ", ")
    }
    printf "}, "
  }
  if(NF == 7){
    printf "{"
    for(i=1;i<=NF-1;i++)
    {
      gsub(/[ \t]+$/, "", $(i));
      gsub(/^[ \t]+/, "", $(i));
      printf "\"%s\": \"%s\"%s",names2[i],($i),(i == NF -1  ? "" : ", ")
    }
    printf "}, "
  }


}
END{
  printf " {\"_x_\": \"_x_\"}]"  # the last item will have a comma after it;
                        # you need an element without a comma after
                        # you could also repeat the print block from the main loop
                        # without the very last comma;
                        # then it'd run on only the last line
}
