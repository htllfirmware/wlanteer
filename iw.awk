
$1 == "Interface" {
    INTERFACE = $2
    cmd = "ethtool -P "$2
    cmd | getline p
    gsub("Permanent address: ","",p)
    INTERFACES[$2]["mac"] = p
    close(cmd)
}
#$1 == "addr" {
# if( ! INTERFACES[INTERFACE]["mac"] ) {
#  INTERFACES[INTERFACE]["mac"] = $2
# }
#}
$1 == "txpower" {
 if( ! INTERFACES[INTERFACE]["power"] ) {
  INTERFACES[INTERFACE]["power"] = $2
 }
}

END {
    printf "%s,%s,%s\n","interface","mac","power"

    for (i in INTERFACES) {
        	printf "%s,%s,%s\n",i,INTERFACES[i]["mac"],INTERFACES[i]["power"]

    }
}
