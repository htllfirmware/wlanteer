import config from 'config';
import Telegram  from './lib/telegram';
import telegramCommandProcessing  from './lib/telegramCommandProcessing';
import Wlan from './lib/wlan';
import S3tools from './lib/s3tools';
import State from './lib/state';
const wlanteerConfig = config.get<WlanteerConfig>('wlanteer');

const main = (async () => {
  const state = new State();
  const myS3 = new S3tools(wlanteerConfig.accessKeyId, wlanteerConfig.secretAccessKey, wlanteerConfig.bucket);
  const passiveWlan = new Wlan(wlanteerConfig.passiveWlan, state);
  const activeWlan = new Wlan(wlanteerConfig.activeWlan, state);
  const telegram = new Telegram(wlanteerConfig.telegramToken, wlanteerConfig.telegramId,
      (_telegram: Telegram) => telegramCommandProcessing(_telegram, passiveWlan, activeWlan, myS3)
    );
  await telegram.sendMessage('wlanteer is online');
  console.log('wlanteer is online');
});
main();
