interface WlanteerConfig {
  accessKeyId: string;
  secretAccessKey: string;
  bucket: string;
  passiveWlan: string;
  activeWlan: string;
  telegramToken: string;
  telegramId: string | number;
}
