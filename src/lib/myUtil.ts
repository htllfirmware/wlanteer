export function sleep(delay: number) {
  return new Promise((res: any) => {
    setTimeout(res, delay);
  });
}
