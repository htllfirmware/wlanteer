export default class State {
  networks: { [index: string]: any};
  wild: boolean;
  constructor() {
    this.networks = {};
    this.wild = false;
  }
  mergeNetworks(networks: any[string]) {
    for (const  net of Object.keys(networks)) {
      if (!this.networks[net]) {
        this.networks[net] = networks[net];
      }
    }
  }
}
