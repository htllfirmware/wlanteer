
import AWS from 'aws-sdk';
import S3 from 'aws-sdk/clients/s3';
import fs from 'fs';

export default class S3tools {
  bucket: string;
  secretKey: string;
  s3: S3;
  constructor(accessKeyId: string, secretAccessKey: string, bucket: string) {
    this.bucket = bucket;
    AWS.config.update({
      accessKeyId, secretAccessKey
    });
    this.s3 = new S3();
  }
  s3upload(file: string, key: string) {
    return new Promise((res: any, rej: any) => {
      fs.readFile(file, (err:  NodeJS.ErrnoException, data: any) => {
        if (err) { rej(err); }
        const base64data = new Buffer(data, 'binary');
        const upload = new S3.ManagedUpload({
          params: {
            Bucket: this.bucket, Key: key, Body: base64data
          }
        });
        upload.send((_err: AWS.AWSError, _data: AWS.S3.ManagedUpload.SendData) => {
          if (err) { rej(err); }
          res(data);
        });
      });
    });
  }
}
