BEGIN{
  FS=","
  OFS=""
  getline #BEGIN runs before anything else, so grab the first line with the titles right away
  for(i=1;i<=NF;i++)
  names[i] = ($i)
  printf "["
}
{
  printf "{"
  for(i=1;i<=NF;i++)
  {
    printf "\"%s\": \"%s\"%s",names[i],($i),(i == NF ? "" : ", ")
  }
  printf "}, "
}
END{
  printf " {\"essid\": \"_x_\"}]"
}
