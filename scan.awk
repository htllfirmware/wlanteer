$1 == "BSS" {
	gsub(".on","",$2)
    MAC = $2
    wifi[MAC]["enc"] = "Open"
}
$1 == "SSID:" {
    gsub("\\\\x","x",$2)
    wifi[MAC]["SSID"] = $2
}
$1 == "freq:" {
    wifi[MAC]["freq"] = $NF
}
$1 == "signal:" {
    wifi[MAC]["sig"] = $2
}
$3 == "set:" && $4 == "channel" {
    wifi[MAC]["chan"] = $5
}
$1 == "WPA:" {
    wifi[MAC]["enc"] = "WPA"
}
$1 == "WEP:" {
    wifi[MAC]["enc"] = "WEP"
}
END {
    printf "%s,%s,%s,%s,%s\n","essid","bssid","channel","signal","encryption"

    for (w in wifi) {
	if (wifi[w]["SSID"] == "")
	{
		continue
	}
        if (wifi[w]["signal"] == "")
        {
                wifi[w]["signal"] = "-99"
        }

        	printf "%s,%s,%s,%s,%s\n",wifi[w]["SSID"],w,wifi[w]["chan"],wifi[w]["sig"],wifi[w]["enc"]

    }
}
