#!/usr/bin/env bash
sudo apt install -y aircrack-ng gawk git python-pip python-m2crypto libnl-dev libgcrypt11-dev udev
pip install future attrs
git clone git://git.kernel.org/pub/scm/linux/kernel/git/sforshee/wireless-regdb.git
git clone git://git.kernel.org/pub/scm/linux/kernel/git/mcgrof/crda.git
echo 'KERNEL=="regulatory*", ACTION=="change", SUBSYSTEM=="platform", RUN+="/sbin/crda"' >> /etc/udev/rules.d/99-com.rules
cd wireless-regdb/
sed -i 's/20/33/g' db.txt
sed -i 's/~\//.\//g' Makefile
make 2>&1 > /var/log/install.log
make install 2>&1 >> /var/log/install.log
cp regulatory.bin /lib/crda/
rm -rf ../crda/pubkeys/*
cp -v *.key.pub.pem ../crda/pubkeys/
cd ../crda
make 2>&1 >> /var/log/install.log
make install 2>&1 >> /var/log/install.log
cd ~
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - 2>&1
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential
cd /usr/src/payload
npm install
npm build
